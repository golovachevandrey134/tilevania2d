using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPIckup : MonoBehaviour
{
    [SerializeField] AudioClip coinPickupSFX;
    [SerializeField] int coinValue = 100;

    bool wasCollected = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && !wasCollected)
        {
            wasCollected = true; // prevent double collect
            AudioSource.PlayClipAtPoint(coinPickupSFX, Camera.main.transform.position);
            FindObjectOfType<GameSession>().AddToScore(coinValue);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}
