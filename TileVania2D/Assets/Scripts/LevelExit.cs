using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
    [SerializeField] float delay = 1f;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(LoadNextLevel());
        }
    }

    IEnumerator LoadNextLevel()
    {
        yield return new WaitForSecondsRealtime(delay);
        
        var sceneIndex = SceneManager.GetActiveScene().buildIndex;

        if (sceneIndex > SceneManager.sceneCountInBuildSettings)
        {
            sceneIndex = 0;
        }
        else
        {
            sceneIndex++;
        }        

        FindObjectOfType<ScenePersist>().ResetScenePersist();
        SceneManager.LoadScene(sceneIndex);
    }
}
